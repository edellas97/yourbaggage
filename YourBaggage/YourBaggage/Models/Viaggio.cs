﻿using System;
using System.Collections.Generic;

namespace yourBaggage.Models
{
    public class Viaggio
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Destinazione { get; set; }
        public DateTime Andata { get; set; }
        public DateTime Ritorno { get; set; }
        public List<Categoria> Categorie { get; set; } = new List<Categoria>();
    }
}