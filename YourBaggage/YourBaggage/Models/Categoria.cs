﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace yourBaggage.Models
{
    public class Categoria
    {
        public Categoria(string nome)
        {
            Id = Guid.NewGuid().ToString();
            Nome = nome;
            Elementi = new List<Elemento>();
        }

        [JsonIgnore]
        public string Id { get; set; }

        public string Nome { get; set; }

        public List<Elemento> Elementi { get; set; }
    }
}