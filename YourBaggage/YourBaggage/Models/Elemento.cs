﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using yourBaggage.DatabaseHelper;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace yourBaggage.Models
{
    public class Elemento : INotifyPropertyChanged
    {
        private bool _inserito;

        [JsonIgnore] public string Id { get; set; } = Guid.NewGuid().ToString();

        public string Nome { get; set; }

        [JsonIgnore]
        public bool Inserito
        {
            get => _inserito;
            set
            {
                _inserito = value;
                OnPropertyChanged(nameof(Inserito));
                _ = DataStore.AggiornaElemento(this);
            }
        }

        [JsonIgnore][NotMapped] public bool IsAbilitato { get; set; } = true;
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}