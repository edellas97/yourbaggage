﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using yourBaggage.Models;

namespace yourBaggage.DatabaseHelper
{
    public static class DataStore
    {
        public static async Task AggiungiViaggio(Viaggio viaggio)
        {
            var viaggioContext = new ViaggioContext();
            viaggioContext.Add(viaggio);
            await viaggioContext.SaveChangesAsync();
        }

        public static async Task<IEnumerable<Viaggio>> GetViaggi()
        {
            var viaggioContext = new ViaggioContext();
            var viaggiInseriti = viaggioContext.Viaggi
                .Include(t => t.Categorie)
                .ThenInclude(c => c.Elementi)
                .AsNoTracking()
                .ToList();
            return await Task.FromResult(viaggiInseriti);
        }

        public static async Task AggiornaViaggio(Viaggio viaggio)
        {
            var viaggioContext = new ViaggioContext();
            viaggioContext.Update(viaggio);
            await viaggioContext.SaveChangesAsync();
        }

        public static async Task AggiornaCheckListViaggio(Viaggio viaggio)
        {
            var viaggioContext = new ViaggioContext();
            var _viaggio = await viaggioContext.Viaggi.FindAsync(viaggio.Id);
            var categorie = await GetCategorie(viaggio.Id);

            foreach (var categoria in viaggio.Categorie)
                if (!categorie.Any(x => x.Nome.Equals(categoria.Nome)))
                    _viaggio.Categorie.Add(categoria);
            viaggioContext.Update(_viaggio);
            await viaggioContext.SaveChangesAsync();
        }

        public static async Task EliminaChecklist(string id)
        {
            var viaggioContext = new ViaggioContext();
            var daEliminare = viaggioContext.Checklists
                .Where(c => c.Id.Equals(id))
                .Include(i => i.Elementi)
                .SingleOrDefault();
            viaggioContext.Checklists.Remove(daEliminare);
            await viaggioContext.SaveChangesAsync();
        }

        public static async Task EliminaViaggio(string id)
        {
            var viaggioContext = new ViaggioContext();
            var daEliminare = viaggioContext.Viaggi
                .Where(x => x.Id.Equals(id))
                .Include(y => y.Categorie)
                .ThenInclude(z => z.Elementi)
                .SingleOrDefault();
            viaggioContext.Viaggi.RemoveRange(daEliminare);
            await viaggioContext.SaveChangesAsync();
        }

        public static async Task<IEnumerable<Categoria>> GetCategorie(string id)
        {
            var viaggioContext = new ViaggioContext();
            var categorie = viaggioContext.Viaggi
                .AsNoTracking()
                .Where(x => x.Id.Equals(id))
                .Include(y => y.Categorie)
                .ThenInclude(z => z.Elementi)
                .Select(c => c.Categorie).SingleOrDefault();
            return await Task.FromResult(categorie);
        }

        public static async Task AggiungiElemento(string idCategoria, Elemento elemento)
        {
            var viaggioContext = new ViaggioContext();
            var categoria = await viaggioContext.Checklists.FindAsync(idCategoria);
            categoria.Elementi.Add(elemento);
            viaggioContext.Update(categoria);
            await viaggioContext.SaveChangesAsync();
        }

        public static async Task AggiornaElemento(Elemento elemento)
        {
            var viaggioContext = new ViaggioContext();
            viaggioContext.Update(elemento);
            await viaggioContext.SaveChangesAsync();
        }

        public static async Task EliminaElemento(Elemento elemento)
        {
            var viaggioContext = new ViaggioContext();
            viaggioContext.ChecklistItems.Remove(elemento);
            await viaggioContext.SaveChangesAsync();
        }
    }
}