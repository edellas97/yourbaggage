
using Microsoft.EntityFrameworkCore;
using SQLitePCL;
using System;
using System.IO;
using Xamarin.Forms;
using yourBaggage.Models;

namespace yourBaggage.DatabaseHelper
{
    public sealed class ViaggioContext : DbContext
    {
        private const string DatabaseName = "database.db";

        public ViaggioContext()
        {
            Database.EnsureCreated();
        }

        public DbSet<Viaggio> Viaggi { get; set; }
        public DbSet<Categoria> Checklists { get; set; }
        public DbSet<Elemento> ChecklistItems { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string databasePath;
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    Batteries_V2.Init();
                    databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..",
                        "Library", DatabaseName);
                    break;
                case Device.Android:
                    databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                        DatabaseName);
                    break;
                default:
                    throw new NotImplementedException("Platform not supported");
            }

            optionsBuilder.UseSqlite($"Filename={databasePath}");
        }
    }
}