﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using yourBaggage.Models;
using yourBaggage.ViewModels;
using Xamarin.Forms;

namespace yourBaggage.DatabaseHelper
{
    public class ViaggioService
    {
        public ViaggioService()
        {
            Viaggi = new ObservableCollection<Viaggio>();
        }

        public ObservableCollection<Viaggio> Viaggi { get; set; }

        public async Task LoadViaggi()
        {
            Viaggi.Clear();
            var trips = await DataStore.GetViaggi();
            foreach (var trip in trips) Viaggi.Add(trip);
        }

        public async Task AddViaggio(Viaggio trip)
        {
            if (!Viaggi.ToList().Any(t => t.Id.Equals(trip.Id)))
            {
                Viaggi.Add(trip);
                await DataStore.AggiungiViaggio(trip);
            }
            else
            {
                await DataStore.AggiornaCheckListViaggio(trip);
            }
        }

        public async Task RemoveViaggio(Viaggio trip)
        {
            Viaggi.Remove(trip);
            await DataStore.EliminaViaggio(trip.Id);
        }

        public async Task UpdateViaggio(Viaggio trip)
        {
            await DataStore.AggiornaViaggio(trip);
            await LoadViaggi();
        }
    }
}
