using System.Threading.Tasks;
using Newtonsoft.Json;
using YourBaggage.Services.HereMaps;

namespace yourBaggage.Services.RestCountries
{
    public class RestCountriesProvider : ApiService
    {
        private const string AllCountriesUrl = "https://restcountries.com/v2/alpha/";
        private const string HereGeocode =
            "https://geocode.search.hereapi.com/v1/geocode?q=";

        private readonly string _hereAutocompleteApiKey = "&apiKey=UD0FgEpC1HriQw7QFALB-qZAi6Uh1F8yALbhzemBm2k";
        public async Task<CountryFacts> GetInfoNazioneAsync(string countrycode)
        {
            var json = await GetAsync(AllCountriesUrl + countrycode);
            return JsonConvert.DeserializeObject<CountryFacts>(json);
        }

        public async Task<Position> GetPosizioneAsync(string cityname)
        {
            var json = await GetAsync(HereGeocode + cityname + _hereAutocompleteApiKey);
            var position = JsonConvert.DeserializeObject<Position>(json);
            return JsonConvert.DeserializeObject<Position>(json);
        }
    }
}