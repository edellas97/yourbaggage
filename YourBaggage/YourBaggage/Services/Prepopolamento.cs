﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using yourBaggage.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace yourBaggage.Services
{
    public static class Prepopolamento
    {
        private const string dbPrepopolato = "YourBaggage.Risorse.JSON.PrepopolamentoCategorie.json";
        private const string dbModificato = "PrepopolamentoCategorieModificato.json";

        public static string PercorsoDB
        {
            get
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..",
                            "Library", dbModificato);
                    case Device.Android:
                        return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                            dbModificato);
                    default:
                        throw new NotImplementedException("Platform not supported");
                }
            }
        }

        public static List<Categoria> GetChecklist()
        {
            return File.Exists(PercorsoDB) ? GetUserCheckList() : GetResourceChecklist();
        }

        private static List<Categoria> GetUserCheckList()
        {
            var jsonContent = File.ReadAllText(PercorsoDB);
            return JsonConvert.DeserializeObject<List<Categoria>>(jsonContent);
        }

        public static List<Categoria> GetResourceChecklist()
        {
            var assembly = typeof(Prepopolamento).GetTypeInfo().Assembly;
            var stream = assembly.GetManifestResourceStream(dbPrepopolato);

            var jsonContent = " ";
            using (var reader = new StreamReader(stream ?? throw new InvalidOperationException()))
            {
                jsonContent = reader.ReadToEnd();
            }

            return JsonConvert.DeserializeObject<List<Categoria>>(jsonContent);
        }

        public static void SetChecklist(List<Categoria> template)
        {
            var json = JsonConvert.SerializeObject(template);
            File.WriteAllText(PercorsoDB, json);
        }
    }
}