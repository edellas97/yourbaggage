

using YourBaggage.Services.HereMaps;

namespace yourBaggage.Services.HereMaps
{
    public class Item
    {
        public Address Address { get; set; }
        public Position Position { get; set; }
    }
}