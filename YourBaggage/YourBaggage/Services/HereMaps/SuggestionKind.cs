namespace yourBaggage.Services.HereMaps
{
    public enum SuggestionKind
    {
        Intersection,
        Street,
        PostalCode,
        District,
        City,
        County,
        State,
        Country
    }
}