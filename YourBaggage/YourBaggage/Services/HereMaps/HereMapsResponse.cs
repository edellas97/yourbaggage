﻿using System.Collections.Generic;
using yourBaggage.Services.HereMaps;

namespace YourBaggage.Services.HereMaps
{
    public class HereMapsResponse
    {
        public List<Suggestion> Suggestions { get; set; }
        public List<Item> Items { get; set; }
    }
}