﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YourBaggage.Services.HereMaps
{
    public class Position
    {
        public string Lat { get; set; }
        public string Lng { get; set; }
    }
}
