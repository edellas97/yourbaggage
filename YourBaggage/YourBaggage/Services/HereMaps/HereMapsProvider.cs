﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using yourBaggage.Services;
using yourBaggage.Services.HereMaps;

namespace YourBaggage.Services.HereMaps
{
    public class HereMapsProvider : ApiService
    {
        private const string HereAutocompleteUrl =
            "https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?&query=";

        private const string HereGeocode =
            "https://geocode.search.hereapi.com/v1/geocode?q=";

        private readonly string _hereAutocompleteApiKey = "&apiKey=UD0FgEpC1HriQw7QFALB-qZAi6Uh1F8yALbhzemBm2k";

    public async Task<List<Suggestion>> GetAutocomplete(string searchText)
        {
            var url = HereAutocompleteUrl + searchText + _hereAutocompleteApiKey;
            var json = await GetAsync(url);

            var hereMapsResponse = JsonConvert.DeserializeObject<HereMapsResponse>(json);

            return hereMapsResponse.Suggestions;
        }

        public async Task<Address> GetGeocode(string searchText)
        {
            var url = HereGeocode + searchText + _hereAutocompleteApiKey;
            var json = await GetAsync(url);

            var hereMapsResponse = JsonConvert.DeserializeObject<HereMapsResponse>(json);

            return hereMapsResponse.Items.FirstOrDefault().Address;
        }

        public async Task<Position> GetGeocodePosition(string searchText)
        {
            var url = HereGeocode + searchText + _hereAutocompleteApiKey;
            var json = await GetAsync(url);


            var hereMapsResponse = JsonConvert.DeserializeObject<HereMapsResponse>(json);

            return hereMapsResponse.Items.FirstOrDefault().Position;
        }
    }
}