﻿using MonkeyCache.FileStore;
using Xamarin.Forms;
using YourBaggage;

namespace yourBaggage
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Barrel.ApplicationId = "MonkeyCacheDemo";
            Barrel.Current.EmptyExpired();

            MainPage = new NavigationPage(new MainPage());
        }

        public static double ScreenHeight { get; set; }
        public static double ScreenWidth { get; set; }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}