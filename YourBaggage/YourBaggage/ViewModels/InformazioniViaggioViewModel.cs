﻿using System.Threading.Tasks;
using yourBaggage.DatabaseHelper;
using yourBaggage.Models;
using yourBaggage.Services.RestCountries;
using YourBaggage.Services.HereMaps;

namespace yourBaggage.ViewModels
{
    public class InformazioniViaggioViewModel : DefaultViewModel
    {
        private readonly HereMapsProvider _hereMapsProvider = new HereMapsProvider();
        private readonly RestCountriesProvider _restCountriesProvider = new RestCountriesProvider();
        private CountryFacts _facts;
        private Position _position;
        private string _tripName;

        public InformazioniViaggioViewModel(Viaggio viaggio, ViaggioService tripsHolder)
        {
            Title = viaggio.Destinazione;
            NomeViaggio = viaggio.Destinazione;
            Viaggio = viaggio;
            TripsHolder = tripsHolder;
            _ = ExecuteLoadInfoCommand();
            _ = ExecuteLoadPositionCommand();
        }

        public ViaggioService TripsHolder { get; set; }

        public Viaggio Viaggio { get; set; }

        public CountryFacts Facts
        {
            get => _facts;
            set => SetProperty(ref _facts, value);
        }

        public Position Position
        {
            get => _position;
            set => SetProperty(ref _position, value);
        }

        public string NomeViaggio
        {
            get => _tripName;
            set => SetProperty(ref _tripName, value);
        }

        private async Task ExecuteLoadInfoCommand()
        {
            var address = await _hereMapsProvider.GetGeocode(Viaggio.Destinazione);
            if (!string.IsNullOrEmpty(address.CountryCode))
            {
                _ = LoadFacts(address.CountryCode);
            }
        }

        private async Task ExecuteLoadPositionCommand()
        {
            var address = await _hereMapsProvider.GetGeocode(Viaggio.Destinazione);
            if (!string.IsNullOrEmpty(address.CountryCode))
            {
                _ = LoadPosition(address.City);
            }
        }

        private async Task LoadFacts(string countryCode)
        {
            Facts = await _restCountriesProvider.GetInfoNazioneAsync(countryCode);
        }

        private async Task LoadPosition(string cityName)
        {
            Position = await _restCountriesProvider.GetPosizioneAsync(cityName);
        }
    }
}