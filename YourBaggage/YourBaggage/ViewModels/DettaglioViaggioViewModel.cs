﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using yourBaggage.DatabaseHelper;
using yourBaggage.Models;
using Xamarin.Forms;



namespace yourBaggage.ViewModels
{
    public class DettaglioViaggioViewModel : DefaultViewModel
    {
        private string _nomeViaggio;



        public DettaglioViaggioViewModel(Viaggio viaggio, ViaggioService viaggioService)
        {
            Title = viaggio.Destinazione;
            NomeViaggio = viaggio.Destinazione;
            Viaggio = viaggio;
            ViaggioService = viaggioService;
            ChecklistGroups = new ObservableCollection<RaggruppamentoDiListe>();
            ComandoCaricaCheckList = new Command(async () => await EseguiComandoCaricaCheckList());
            ComandoEliminaCheckList = new Command<Elemento>(async elemento => await EseguiComandoEliminaElemento(elemento));
            ComandoAggiungiElemento = new Command<RaggruppamentoDiListe>(async groupedList => await EseguiComandoAggiungiElemento(groupedList));
            ComandoEliminaCategoria = new Command<RaggruppamentoDiListe>(async groupedList => await EseguiComandoEliminaCategoria(groupedList));
            MessagingCenter.Subscribe<SceltaCategoriaViewModel, Viaggio>(this, "SetupTrip",async (obj, trip) => { await EseguiComandoCaricaCheckList(); });
            _ = EseguiComandoCaricaCheckList(true);
        }

        public ViaggioService ViaggioService { get; set; }
        public Command ComandoCaricaCheckList { get; set; }
        public Command<Elemento> ComandoEliminaCheckList { get; set; }
        public Command<RaggruppamentoDiListe> ComandoAggiungiElemento { get; set; }
        public Command<RaggruppamentoDiListe> ComandoEliminaCategoria { get; set; }
        public Viaggio Viaggio { get; set; }
        public ObservableCollection<RaggruppamentoDiListe> ChecklistGroups { get; set; }



        public string NomeViaggio
        {
            get => _nomeViaggio;
            set => SetProperty(ref _nomeViaggio, value);
        }

        private async Task EseguiComandoCaricaCheckList(bool forceRefresh = false)
        {
            IsBusy = true;
            var categorie = forceRefresh ? await DataStore.GetCategorie(Viaggio.Id) : Viaggio.Categorie;
            ChecklistGroups.Clear();

            foreach (var categoria in categorie)
            {
                var listGroup = new RaggruppamentoDiListe
                {
                    Id = categoria.Id,
                    NomeGruppo = categoria.Nome
                };
                foreach (var elemento in categoria.Elementi) listGroup.Add(elemento);
                ChecklistGroups.Add(listGroup);
            }
            IsBusy = false;
        }

        private async Task EseguiComandoEliminaElemento(Elemento elemento)
        {
            await DataStore.EliminaElemento(elemento);
            for (var i = ChecklistGroups.Count - 1; i >= 0; i--)
            {
                ChecklistGroups[i].Remove(elemento);
                if (ChecklistGroups[i].Count == 0) ChecklistGroups.Remove(ChecklistGroups[i]);
            }
        }

        public async Task EseguiComandoAggiungiElemento(RaggruppamentoDiListe groupedList)
        {
            var elementoName = await Application.Current
            .MainPage.DisplayPromptAsync("Aggiungi Elemento", "Inserisci nome dell'elemento:");
            if (!string.IsNullOrEmpty(elementoName))
            {
                var newElemento = new Elemento { Nome = elementoName };
                groupedList.Insert(0, newElemento);
                await DataStore.AggiungiElemento(groupedList.Id, newElemento);
            }
        }

        public async Task EseguiComandoEliminaCategoria(RaggruppamentoDiListe groupedList)
        {
            ChecklistGroups.Remove(groupedList);
            Viaggio.Categorie.RemoveAll(c => c.Id == groupedList.Id);
            await DataStore.EliminaChecklist(groupedList.Id);
        }

        internal void AggiungiCategoria(string name)
        {
            var category = new Categoria(name);
            Viaggio.Categorie.Add(category);
            var listGroup = new RaggruppamentoDiListe
            {
                Id = category.Id,
                NomeGruppo = category.Nome
            };
            ChecklistGroups.Add(listGroup);
            _ = DataStore.AggiornaCheckListViaggio(Viaggio);
        }
    }
}