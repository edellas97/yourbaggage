﻿using System.Threading.Tasks;
using yourBaggage.DatabaseHelper;
using yourBaggage.Models;
using yourBaggage.Services.RestCountries;
using YourBaggage.Services.HereMaps;

namespace yourBaggage.ViewModels
{
    public class MappaViewModel : DefaultViewModel
    {
        private readonly HereMapsProvider _hereMapsProvider = new HereMapsProvider();
        private readonly RestCountriesProvider _restCountriesProvider = new RestCountriesProvider();
        private Position _position;
        private string _tripName;

        public MappaViewModel(Viaggio viaggio, ViaggioService tripsHolder)
        {
            Title = viaggio.Destinazione;
            NomeViaggio = viaggio.Destinazione;
            Viaggio = viaggio;
            TripsHolder = tripsHolder;
            _ = ExecuteLoadPositionCommand();
        }

        public ViaggioService TripsHolder { get; set; }

        public Viaggio Viaggio { get; set; }

        public Position Position
        {
            get => _position;
            set => SetProperty(ref _position, value);
        }

        public string NomeViaggio
        {
            get => _tripName;
            set => SetProperty(ref _tripName, value);
        }

        private async Task ExecuteLoadPositionCommand()
        {
            var address = await _hereMapsProvider.GetGeocodePosition(Viaggio.Destinazione);
            if (!string.IsNullOrEmpty(address.Lng) && !string.IsNullOrEmpty(address.Lat))
            {
                _ = LoadPosition(Viaggio.Destinazione);
            }
        }

        public async Task LoadPosition(string cityName)
        {
            Position = await _restCountriesProvider.GetPosizioneAsync(cityName);
        }
    }
}