﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using yourBaggage.DatabaseHelper;
using yourBaggage.Models;
using yourBaggage.Views;
using Microsoft.AppCenter.Crashes;
using Xamarin.Forms;



namespace yourBaggage.ViewModels
{
    public class ViaggiViewModel : DefaultViewModel
    {
        public Command ComandoCaricaViaggi { get; set; }
        public Command<Viaggio> ComandoEliminaViaggio { get; set; }
        public ViaggioService ViaggioService { get; set; }
        public ViaggiViewModel()
        {
            Title = "Viaggi";
            ComandoCaricaViaggi = new Command(async () => await EseguiCaricaViaggi());
            ComandoEliminaViaggio = new Command<Viaggio>(async viaggio => await EseguiEliminaViaggio(viaggio));
            ViaggioService = new ViaggioService();
            _ = ViaggioService.LoadViaggi();
        }

        public async Task EseguiCaricaViaggi()
        {
            IsBusy = true;
            await ViaggioService.LoadViaggi();
            IsBusy = false;
        }

        private async Task EseguiEliminaViaggio(Viaggio viaggio)
        {
            await ViaggioService.RemoveViaggio(viaggio);
        }
    }
}