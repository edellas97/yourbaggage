﻿using System;
using System.Collections.Generic;
using yourBaggage.Views;
using Xamarin.Forms;
using yourBaggage.DatabaseHelper;
using yourBaggage.Models;
using YourBaggage.Services.HereMaps;
using yourBaggage.Services.HereMaps;

namespace yourBaggage.ViewModels
{
    public class AggiungiViaggioViewModel : DefaultViewModel
    {
        private string _destinazione = "Destinazione";
        private DateTime _ritorno = DateTime.Now;
        private DateTime _andata = DateTime.Now;
        public AggiungiViaggioViewModel(ViaggioService viaggioService)
        {
            ViaggioService = viaggioService;
            NuovoViaggio = new Viaggio
            {
                Destinazione = "Destinazione",
                Andata = _andata,
                Ritorno = _ritorno
            };
            SubscribeToMessagingCenter();
        }
        public Viaggio NuovoViaggio { get; }
        public ViaggioService ViaggioService { get; set; }
        public DateTime Andata
        {
            get => _andata;
            set
            {
                SetProperty(ref _andata, value);
                NuovoViaggio.Andata = value;
            }
        }
        public DateTime Ritorno
        {
            get => _ritorno;
            set
            {
                SetProperty(ref _ritorno, value);
                NuovoViaggio.Ritorno = value;
            }
        }
        public string Destinazione
        {
            get => _destinazione;
            set
            {
                SetProperty(ref _destinazione, value);
                NuovoViaggio.Destinazione = value;
            }
        }
        private void SubscribeToMessagingCenter()
        {
            MessagingCenter.Subscribe<CercaDestinazione,Suggestion>(this, "ItemSelected",
            (obj, selected) =>
            {
                if (!string.IsNullOrEmpty(selected.Address.City))
                    Destinazione = selected.Address.City;
                else if (!string.IsNullOrEmpty(selected.Address.State))
                    Destinazione = selected.Address.State;
                else if (!string.IsNullOrEmpty(selected.Address.CountryName))
                    Destinazione = selected.Address.CountryName;
                else
                    Destinazione = selected.Label;
            });
        }
    }
}