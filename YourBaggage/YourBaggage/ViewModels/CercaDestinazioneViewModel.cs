using System.Collections.ObjectModel;
using System.Threading.Tasks;
using yourBaggage.Services;
using yourBaggage.Services.HereMaps;
using YourBaggage.Services.HereMaps;

namespace yourBaggage.ViewModels
{
    public class CercaDestinazioneViewModel : DefaultViewModel
    {
        private readonly HereMapsProvider _hereMapsProvider = new HereMapsProvider();
        private readonly ElementiCoda _elementiCoda = new ElementiCoda();
        private string _searchText;

        public string SearchText
        {
            get => _searchText;
            set
            {
                SetProperty(ref _searchText, value);
                _ = _elementiCoda.Enqueue(GetAutoCompletamentiAsync);
            }
        }

        public ObservableCollection<Suggestion> SearchAutoCompletamenti { get; set; } =
            new ObservableCollection<Suggestion>();

        private async Task GetAutoCompletamentiAsync()
        {
            SearchAutoCompletamenti.Clear();
            SearchAutoCompletamenti.Add(new Suggestion {Label = _searchText});

            var autocompletamenti = await _hereMapsProvider.GetAutocomplete(_searchText);

            foreach (var autocompletamento in autocompletamenti)
                switch (autocompletamento.MatchLevel)
                {
                    case SuggestionKind.City:
                        SearchAutoCompletamenti.Add(autocompletamento);
                        break;
                    case SuggestionKind.County:
                        SearchAutoCompletamenti.Add(autocompletamento);
                        break;
                    case SuggestionKind.State:
                        SearchAutoCompletamenti.Add(autocompletamento);
                        break;
                }
        }
    }
}