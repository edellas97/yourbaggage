﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using yourBaggage.DatabaseHelper;
using yourBaggage.Models;
using yourBaggage.Services;
using MLToolkit.Forms.SwipeCardView.Core;
using Xamarin.Forms;
using YourBaggage;

namespace yourBaggage.ViewModels
{
    public class SceltaCategoriaViewModel : DefaultViewModel
    {
        private readonly List<Elemento> _existingItems = new List<Elemento>();
        private uint _sogliaScorrimento;



        public SceltaCategoriaViewModel(INavigation navigation, Viaggio viaggio, ViaggioService viaggioService)
        {
            Navigation = navigation;
            Viaggio = viaggio;
            ViaggioService = viaggioService;
            SwipedCommand = new Command<SwipedCardEventArgs>(eventArgs => EseguiComandoSwipe(eventArgs));
            SogliaScorrimento = (uint)(App.ScreenWidth / 3);
            Categorie = Prepopolamento.GetChecklist();
            Categorie.RemoveAll(x => Viaggio.Categorie.Exists(y => y.Nome.Equals(x.Nome)));
            CategorieSelezionate = new List<Categoria>();
        }



        public INavigation Navigation { get; set; }

        public ViaggioService ViaggioService { get; set; }

        public Viaggio Viaggio { get; set; }

        public List<Categoria> Categorie { get; set; }

        public List<Categoria> CategorieSelezionate { get; set; }

        public SwipeCardDirection SupportedSwipeDirections => SwipeCardDirection.Right | SwipeCardDirection.Left;

        public ICommand SwipedCommand { get; }

        public uint SogliaScorrimento
        {
            get => _sogliaScorrimento;
            set => SetProperty(ref _sogliaScorrimento, value);
        }

        private void EseguiComandoSwipe(SwipedCardEventArgs eventArgs)
        {
            var selezionato = eventArgs.Item as Categoria;

            if (eventArgs.Direction == SwipeCardDirection.Right)
            {
                selezionato.Elementi.RemoveAll(x => x.IsAbilitato == false);
                AddCategory(selezionato);
            }
            if (selezionato == Categorie.Last()) _ = CompletaViaggio();
        }

        internal async Task CompletaViaggio()
        {
            Viaggio.Categorie.AddRange(CategorieSelezionate);
            await ViaggioService.AddViaggio(Viaggio);
            _ = Navigation.PushModalAsync(new NavigationPage(new MainPage()));
        }

        private void AddCategory(Categoria categoria)
        {
            categoria.Elementi.RemoveAll(x => _existingItems.Exists(y => y.Nome.Equals(x.Nome)));
            _existingItems.AddRange(categoria.Elementi);
            CategorieSelezionate.Add(categoria);
        }
    }
}