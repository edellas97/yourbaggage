﻿using System.Collections.ObjectModel;
using yourBaggage.Models;

namespace yourBaggage.ViewModels
{
    public class RaggruppamentoDiListe : ObservableCollection<Elemento>
    {
        public string Id { get; set; }
        public string NomeGruppo { get; set; }
        public ObservableCollection<Elemento> Elementi { get; set; } = new ObservableCollection<Elemento>();
    }
}