﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using yourBaggage.Models;
using Xamarin.Forms;

namespace yourBaggage.ViewModels
{
    public class MainViewModel : DefaultViewModel
    {
        public MainViewModel()
        {
            Title = "Viaggi";
        }
        public Command ComandoCaricaViaggi { get; set; }

        public Command<Viaggio> ComandoEliminaViaggio { get; set; }

    }
}