﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using yourBaggage.Models;
using Microsoft.AppCenter.Crashes;
using Xamarin.Forms;
using yourBaggage.Services;

namespace yourBaggage.ViewModels
{
    public class ModificaCategorieViewModel : DefaultViewModel
    {
        public ModificaCategorieViewModel()
        {
            ComandoAggiungiElemento = new Command<RaggruppamentoDiListe>(async groupedList => await EseguiAggiungiElemento(groupedList));
            ComandoEliminaElemento = new Command<Elemento>(EseguiEliminaElemento);
            ComandoAggiungiCategoria = new Command(async () => await EseguiAggiungiCategoria());

            EseguiCaricaCheckList();
        }

        public ObservableCollection<RaggruppamentoDiListe> ChecklistGroups { get; set; } = new ObservableCollection<RaggruppamentoDiListe>();

        public Command<RaggruppamentoDiListe> ComandoAggiungiElemento { get; set; }
        public Command<Elemento> ComandoEliminaElemento { get; set; }

        public Command ComandoAggiungiCategoria { get; set; }

        private async Task EseguiAggiungiCategoria()
        {
            var nomeCategoria = await Application.Current
            .MainPage.DisplayPromptAsync("Aggiungi una categoria", "Inserisci il nome:");



            if (!string.IsNullOrEmpty(nomeCategoria) && !IsPresente(nomeCategoria))
            {
                ChecklistGroups.Insert(0, new RaggruppamentoDiListe
                {
                    NomeGruppo = nomeCategoria,
                    Elementi = new ObservableCollection<Elemento>()
                });
                SaveCategoria();
            }
            else
            {
                await Application.Current
                .MainPage.DisplayAlert("Attenzione", "Non puoi inserire una categoria gia presente o senza nome!", "Annulla");
            }
        }



        private bool IsPresente(string nomeCategoria)
        {
            var checklists = Prepopolamento.GetChecklist();



            foreach (var category in checklists)
            {
                if (nomeCategoria.Equals(category.Nome))
                    return true;
            }
            return false;
        }

        private void EseguiCaricaCheckList()
        {
            IsBusy = true;
            ChecklistGroups.Clear();
            try
            {
                var checklists = Prepopolamento.GetChecklist();

                foreach (var category in checklists)
                {
                    var listGroup = new RaggruppamentoDiListe
                    {
                        NomeGruppo = category.Nome
                    };
                    foreach (var item in category.Elementi) listGroup.Add(item);
                    ChecklistGroups.Add(listGroup);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                Crashes.TrackError(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void EseguiEliminaElemento(Elemento elemento)
        {
            for (var i = ChecklistGroups.Count - 1; i >= 0; i--)
            {
                ChecklistGroups[i].Remove(elemento);
                if (ChecklistGroups[i].Count == 0) ChecklistGroups.Remove(ChecklistGroups[i]);
            }

            SaveCategoria();
        }

        public async Task EseguiAggiungiElemento(RaggruppamentoDiListe raggruppamentoDiListe)
        {
            var nome = await Application.Current
                .MainPage.DisplayPromptAsync("Aggiungi elemento alla categoria", "Inserisci il nome:");
            if (!string.IsNullOrEmpty(nome))
            {
                var nuovoElemento = new Elemento { Nome = nome };
                raggruppamentoDiListe.Insert(0, nuovoElemento);
                SaveCategoria();
            }
        }

        private void SaveCategoria()
        {
            var nuovaCategoria = new List<Categoria>();
            foreach (var group in ChecklistGroups)
                nuovaCategoria.Add(new Categoria(group.NomeGruppo)
                {
                    Elementi = group.ToList()
                });

            Prepopolamento.SetChecklist(nuovaCategoria);
        }
    }
}