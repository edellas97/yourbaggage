﻿using System;
using yourBaggage.Models;
using yourBaggage.ViewModels;
using MLToolkit.Forms.SwipeCardView.Core;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using MLToolkit.Forms.SwipeCardView;



namespace yourBaggage.Views
{
    public partial class SceltaCategoria : ContentPage
    {
        private readonly SceltaCategoriaViewModel viewModel;



        public SceltaCategoria(SceltaCategoriaViewModel viewModel)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = this.viewModel = viewModel;
            SwipeCardView.Dragging += OnDragging;
        }



        private void Done_Clicked(object sender, EventArgs e)
        {
            _ = viewModel.CompletaViaggio();
        }

        private void Close_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }


        private void OnDragging(object sender, DraggingCardEventArgs e)
        {
            var view = (View)sender;



            var nopeFrame = view.FindByName<Frame>("NopeFrame");
            var likeFrame = view.FindByName<Frame>("AddFrame");



            var sogliaScorrimento = (BindingContext as SceltaCategoriaViewModel).SogliaScorrimento;
            var draggedXPercent = e.DistanceDraggedX / sogliaScorrimento;



            switch (e.Position)
            {
                case DraggingCardPosition.Start:
                    view.BackgroundColor = Color.FromHex("#2196F3");
                    nopeFrame.Opacity = 0;
                    likeFrame.Opacity = 0;
                    break;



                case DraggingCardPosition.UnderThreshold:
                    view.BackgroundColor = Color.FromHex("#2196F3");
                    if (e.Direction == SwipeCardDirection.Left)
                    {
                        nopeFrame.Opacity = -1 * draggedXPercent;
                    }
                    else if (e.Direction == SwipeCardDirection.Right)
                    {
                        likeFrame.Opacity = draggedXPercent;
                    }
                    else if (e.Direction == SwipeCardDirection.Up)
                    {
                        nopeFrame.Opacity = 0;
                        likeFrame.Opacity = 0;
                    }



                    break;



                case DraggingCardPosition.OverThreshold:
                    switch (e.Direction)
                    {
                        case SwipeCardDirection.Left:
                            view.BackgroundColor = Color.FromHex("#FF6A4F");
                            nopeFrame.Opacity = 1;
                            break;


                        case SwipeCardDirection.Right:
                            view.BackgroundColor = Color.FromHex("#63DD99");
                            likeFrame.Opacity = 1;
                            break;



                        case SwipeCardDirection.Up:
                            view.BackgroundColor = Color.FromHex("#2196F3");
                            nopeFrame.Opacity = 0;
                            likeFrame.Opacity = 0;
                            break;



                        case SwipeCardDirection.Down:
                            view.BackgroundColor = Color.FromHex("#2196F3");
                            nopeFrame.Opacity = 0;
                            likeFrame.Opacity = 0;
                            break;
                    }



                    break;



                case DraggingCardPosition.FinishedUnderThreshold:
                    view.BackgroundColor = Color.FromHex("#2196F3");
                    nopeFrame.Opacity = 0;
                    likeFrame.Opacity = 0;
                    break;



                case DraggingCardPosition.FinishedOverThreshold:
                    view.BackgroundColor = Color.FromHex("#2196F3");
                    nopeFrame.Opacity = 0;
                    likeFrame.Opacity = 0;
                    break;



                default:
                    throw new ArgumentOutOfRangeException();
            }
        }




        private async void DismissCard_Clicked(object sender, EventArgs e)
        {
            await SwipeCardView.InvokeSwipe(SwipeCardDirection.Left, 20, 10, new TimeSpan(1), new TimeSpan(200));
        }



        private async void AcceptCard_Clicked(object sender, EventArgs e)
        {
            await SwipeCardView.InvokeSwipe(SwipeCardDirection.Right, 20, 10, new TimeSpan(1), new TimeSpan(200));
        }
    }
}