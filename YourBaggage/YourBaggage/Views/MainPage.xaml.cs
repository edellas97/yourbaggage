﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using yourBaggage.Models;
using yourBaggage.ViewModels;
using yourBaggage.Views;

namespace YourBaggage
{
    public partial class MainPage : ContentPage
    {
        private readonly ViaggiViewModel viewModel;
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = viewModel = new ViaggiViewModel();
        }



        private async void BtnAggiungi_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new AggiungiViaggio(new AggiungiViaggioViewModel(viewModel.ViaggioService))));
        }



        private async void BtnCategorie_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new ModificaCategorie(new ModificaCategorieViewModel())));
        }



        private async void OnItemSelected(object sender, EventArgs args)
        {
            var layout = (BindableObject)sender;
            var item = (Viaggio)layout.BindingContext;
            await Navigation.PushModalAsync(new NavigationPage(new DettaglioViaggio(new DettaglioViaggioViewModel(item, viewModel.ViaggioService))));
        }

        private async void Info_Clicked(object sender, EventArgs args)
        {
            var layout = (BindableObject)sender;
            var item = (Viaggio)layout.BindingContext;
            await Navigation.PushModalAsync(new NavigationPage(new InformazioniNazione(new InformazioniViaggioViewModel(item, viewModel.ViaggioService))));
        }

        private async void Vedi_Mappa(object sender, EventArgs e)
        {
            var layout = (BindableObject)sender;
            var item = (Viaggio)layout.BindingContext;
            await Navigation.PushModalAsync(new NavigationPage(new Mappa(new InformazioniViaggioViewModel(item, viewModel.ViaggioService))));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();



            if (viewModel.ViaggioService.Viaggi.Count == 0)
                viewModel.IsBusy = true;
        }
    }
}