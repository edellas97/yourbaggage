﻿using System;
using System.Threading.Tasks;
using yourBaggage.Models;
using yourBaggage.ViewModels;
using Xamarin.Forms;

namespace yourBaggage.Views
{
    public partial class InformazioniNazione
    {
        private readonly InformazioniViaggioViewModel _viewModel;

        public InformazioniNazione(InformazioniViaggioViewModel viewModel)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = _viewModel = viewModel;
        }

        private void Done_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}