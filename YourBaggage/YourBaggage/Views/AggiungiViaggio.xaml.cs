﻿using System;
using yourBaggage.ViewModels;
using Xamarin.Forms;
using YourBaggage;
using YourBaggage.Services.HereMaps;
using yourBaggage.Services.HereMaps;

namespace yourBaggage.Views
{
    public partial class AggiungiViaggio : ContentPage
    {
        private readonly AggiungiViaggioViewModel viewModel;



        public AggiungiViaggio(AggiungiViaggioViewModel viewModel)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = this.viewModel = viewModel;
            MessagingCenter.Subscribe<CercaDestinazione, Suggestion>(this, "ItemSelected",
            (obj, selected) =>
    {
        var searchBar = this.FindByName<Label>("DestinationSearchBar");
        if (!string.IsNullOrEmpty(selected.Address.City))
            searchBar.Text = " " + selected.Address.City;
         else if (!string.IsNullOrEmpty(selected.Address.State))
                searchBar.Text = " " + selected.Address.State;
        else if (!string.IsNullOrEmpty(selected.Address.CountryName))
                searchBar.Text = " " + selected.Address.CountryName;
        else
                searchBar.Text = " " + selected.Label;
            });
        }

        private async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void Next_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
            var setupPage = new SceltaCategoria(new SceltaCategoriaViewModel(Navigation, viewModel.NuovoViaggio, viewModel.ViaggioService));
            await Navigation.PushAsync(setupPage);
        }

        private async void Search_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new CercaDestinazione()));
        }
    }
}