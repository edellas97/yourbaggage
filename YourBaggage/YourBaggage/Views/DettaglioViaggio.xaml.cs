﻿using System;
using System.Threading.Tasks;
using yourBaggage.Models;
using yourBaggage.ViewModels;
using Xamarin.Forms;

namespace yourBaggage.Views
{
    public partial class DettaglioViaggio
    {
        private readonly DettaglioViaggioViewModel _viewModel;

        public DettaglioViaggio(DettaglioViaggioViewModel viewModel)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = _viewModel = viewModel;
        }

        private async void Menu_Clicked(object sender, EventArgs e)
        {
            var action = await DisplayActionSheet("Modifica Viaggio", DialogActions.Annulla,
                DialogActions.Elimina, DialogActions.Rinomina, DialogActions.AggiungiCategoriaEsistente,
                DialogActions.AggiungiCategoria);

            switch (action)
            {
                case DialogActions.Elimina:
                    await DeleteItem_Clicked();
                    break;
                case DialogActions.Rinomina:
                    await RenameItem_Clicked();
                    break;
                case DialogActions.AggiungiCategoriaEsistente:
                    await AddTemplateCategory_Clicked();
                    break;
                case DialogActions.AggiungiCategoria:
                    await AddEmptyCategory_Clicked();
                    break;
            }
        }

        private async Task AddEmptyCategory_Clicked()
        {
            var categoryName = await DisplayPromptAsync("Aggiungi una categoria", "Inserisci il nome:");

            if (!string.IsNullOrEmpty(categoryName)) _viewModel.AggiungiCategoria(categoryName);
        }

        private async Task DeleteItem_Clicked()
        {
            await Navigation.PopModalAsync().ContinueWith(_ => _viewModel.ViaggioService.RemoveViaggio(_viewModel.Viaggio));
        }

        private async Task RenameItem_Clicked()
        {
            var tripName = await DisplayPromptAsync("Rinomina viaggio", "Inserisci il nome:");

            if (!string.IsNullOrEmpty(tripName))
            {
                Title = tripName;
                _viewModel.NomeViaggio = tripName;
                _viewModel.Viaggio.Destinazione = tripName;
                await _viewModel.ViaggioService.UpdateViaggio(_viewModel.Viaggio);
            }
        }

        private async Task AddTemplateCategory_Clicked()
        {
            var setupPage = new SceltaCategoria(new SceltaCategoriaViewModel(Navigation, _viewModel.Viaggio, _viewModel.ViaggioService));
            await Navigation.PushModalAsync(new NavigationPage(setupPage));
        }

        private void Label_Clicked(object sender, EventArgs e)
        {
            if ((sender as Label)?.BindingContext is Elemento item) item.Inserito = !item.Inserito;
        }

        private async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }

    internal static class DialogActions
    {
        public const string Annulla = "Annulla";
        public const string Elimina = "Elimina";
        public const string Rinomina = "Rinomina";
        public const string AggiungiCategoriaEsistente = "Aggiungi una categoria esistente";
        public const string AggiungiCategoria = "Aggiungi una categoria";
    }
}