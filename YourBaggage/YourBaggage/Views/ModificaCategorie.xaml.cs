﻿using System;
using yourBaggage.ViewModels;
using Xamarin.Forms;

namespace yourBaggage.Views
{
    public partial class ModificaCategorie : ContentPage
    {
        private readonly ModificaCategorieViewModel viewModel;

        public ModificaCategorie(ModificaCategorieViewModel viewModel)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = this.viewModel = viewModel;
        }

        private void Done_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}