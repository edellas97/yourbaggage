﻿using System;
using System.Linq;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using yourBaggage.ViewModels;

namespace yourBaggage.Views
{
    public partial class Mappa : ContentPage
    {
        private readonly InformazioniViaggioViewModel _viewModel;
        public Mappa(InformazioniViaggioViewModel viewModel)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = _viewModel = viewModel;
            GetGeocoding();
        }

        public async void GetGeocoding()
        {
            var locations = await Geocoding.GetLocationsAsync(_viewModel.Viaggio.Destinazione);
            var locationDetails = locations?.FirstOrDefault();
            Pin pin = new Pin
            {
                Label = _viewModel.Viaggio.Destinazione,
                Type = PinType.Place,
                Position = new Position(locationDetails.Latitude, locationDetails.Longitude)
            };
            map.Pins.Add(pin);
            map.MoveToRegion(MapSpan.FromCenterAndRadius(
            new Position(locationDetails.Latitude, locationDetails.Longitude), Distance.FromKilometers(1)));
        }

        private void Indietro_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}


