﻿using System;
using YourBaggage.Services.HereMaps;
using yourBaggage.ViewModels;
using Xamarin.Forms;
using yourBaggage.Services.HereMaps;

namespace yourBaggage.Views
{
    public partial class CercaDestinazione : ContentPage
    {
        private readonly CercaDestinazioneViewModel viewModel;

        public CercaDestinazione()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = viewModel = new CercaDestinazioneViewModel();
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selected = (Suggestion) e.Item;
            MessagingCenter.Send(this, "ItemSelected", selected);
            Navigation.PopModalAsync();
        }

        private void Cancel_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}